class LinksController < ApplicationController
  rescue_from URI::InvalidURIError, :with => :invalid_url_msg

  def show
    @link = Link.find_by(slug: params[:slug])
    redirect_to @link.original_url
  end

  def create
    shortener = Shortener.new(link_params[:original_url])
    @link = shortener.generate_slug

    if @link.persisted?
      render :json => @link
    else
      raise URI::InvalidURIError
    end
  end

  private

  def link_params
    params.require(:link).permit(:original_url)
  end

  def invalid_url_msg(error)
    render :json => { :error => error.message }, :status => 500
  end
end
