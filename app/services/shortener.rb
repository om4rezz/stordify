require 'digest/sha2'

class Shortener

  attr_reader :url, :link_model

  def initialize(url, link_model = Link)
    @url = url
    @link_model = link_model
  end

  def generate_slug
    link_model.create(original_url: url, slug: slug)
  end

  def slug
    loop do
      slug = get_fresh_slug
      break slug unless link_model.exists?(slug: slug)
    end
  end

  private

  def get_fresh_slug
    SecureRandom.uuid[0..6]
  end
end
