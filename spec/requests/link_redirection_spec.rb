require 'rails_helper'

RSpec.describe "Link", type: :request do

  it "redirects to the original URL for a given short link" do
    url = "https://twitter.com/om4rezz/status/1333810871798276098"
    shortener = Shortener.new(url)
    link = shortener.generate_slug

    get link.shortened_url

    expect(response).to redirect_to(link.original_url)
  end
end
