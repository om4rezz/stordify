require 'rails_helper'

RSpec.describe Shortener do
  it "shortens a given url to a 7 character slug" do
    url = "https://www.stordify.com/super-duper/users/om4rezz"
    shortener = Shortener.new(url)

    expect(shortener.slug.length).to eq(7)
  end

  it "gives each URL its own slug" do
    url = "https://www.stordify.com/super-duper/users/om4rezz"
    shortener = Shortener.new(url)
    slug_1 = shortener.slug

    url = "https://www.stordify.com/personal-settings/users/rubyist"
    shortener = Shortener.new(url)
    slug_2 = shortener.slug

    expect(slug_1).not_to eq(slug_2)
  end

  it "generate a link record with a unique slug" do
    url = "https://www.stordify.com/super-duper/users/om4rezz"
    shortener = Shortener.new(url)
    link = shortener.generate_slug
    expect(link.valid?).to be(true)
    link_2 = shortener.generate_slug
    expect(link_2.valid?).to be(true)
  end
end