require 'rails_helper'

RSpec.describe Link, type: :model do

  it "is valid if it has an original URL and a slug" do
    link = Link.new(original_url: "https://www.stordify.com/super-duper/users/om4rezz", slug: "1234567")
    expect(link.valid?).to be(true)
  end

  it "is invalid if the original URL is not formatted properly" do
    link = Link.new(original_url: "i_love_coffee", slug: "1234567")
    expect(link.valid?).to be(false)
  end

  it "is invalid if it does not have a slug" do
    link = Link.new(original_url: "https://www.stordify.com/super-duper/users/om4rezz")
    expect(link.valid?).to be(false)
  end

  it "is invalid if it does not have an original URL" do
    link = Link.new(slug: "1234567")
    expect(link.valid?).to be(false)
  end

  it "is invalid if the slug already exists" do
    link_1 = Link.new(original_url: "https://www.stordify.com/super-duper/users/om4rezz", slug: "1234567")
    link_1.save

    link_2 = Link.new(original_url: "https://www.stordify.com/personal-settings/users/rubyist", slug: "1234567")

    expect(link_2.valid?).to be(false)
  end
end
